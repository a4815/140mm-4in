140mm to 4" HVAC metal duct fan adaptor. This fan is typical for Whatsminer Bitcoin ASIC miners. I recommend 6" duct for the M30/M31 series. I have no experience with the M20 so I can't speak to that. 4" works in a pinch with the M30/M31 depending on the layout of the system.

.6mm Nozzle: .65mm wide/.3mm layer height
             3 perimeters/4 top and bottom layers
             No supports, brim or raft
             25% infill
             PETG filament
             Print as fast as your machine allows for good layer adhesion

.4mm Nozzle: .5mm wide/.2mm layer height
             5 perimeters/6 top and bottom layers
             No supports, brim or raft
             25% infill
             PETG
             Print as fast as your machine allows for good layer adhesion             
